# Welcome!

Welcome to the <b>Research Computing Services 3D Slicer Digital Support Pack</b>! This Digital Support Pack contains online resources that are intended to support your learning remotely and to complement our in-person workshops. For more information about the Digital Support Pack, visit [http://go.unimelb.edu.au/e7kr](http://go.unimelb.edu.au/e7kr).

Research Computing Services is currently transitioning to a virtual campus model in line with the University of Melbourne's Covid-19 protocol. For support or information about the university's COVID-19 policies visit [http://www.unimelb.edu.au/coronavirus](http://www.unimelb.edu.au/coronavirus).

This Gitlab is a work in progress. We will be adding resources as we discover new ways to support your learning and research remotely, so please check back from time to time. Happy e-learning!

***

# Course Overview

3D Slicer is a free and open source software package for analysis and visualisation of medical imaging and 3D data. The program can be used for medical education, patient education, surgical planning, and 3D printing and simulation, among other things. This program is especially useful if you work with medical images or 3D data, for example in engineering, biomedical sciences, vetinary sciences, or medicine and dentistry. 



### Learning Objectives
This introductory course will familiarise you with the basics of the program, and equip you with the skills you need to undertake your own projects. It may help you with the research skills of scientific education and communication, data visualisation, and prototyping.

You will:
*  understand how 3D Slicer can be used for projects and research
*  understand the principles of medical imaging and 3D datasets
*  learn how to visualise data using the volume rendering module
*  learn how to create and clean up segments using the segment editor module
*  know where to find extension modules and additional resources


### Eligibility and Requirements
No prior knowledge required. This course is aimed at beginners, and all are welcome!


### Assessment
You will be able to test your knowledge by attempting the in-class challenges.

### Dates and Times
As part of our transition to e-learning, we will also be offering workshops, drop-in sessions and support over Zoom. You can find the dates and times on the [Research Computing Services Eventbrite](http://rescomunimelb.eventbrite.com.au/) page.


# Course Structure

This course is designed to be worked through either at home, or through online Zoom workshops. The resources may be accessed in the following order:

0. [**Home**](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro) 
<br>This is the home page for our course. It describes the course objectives and structure.</br>

1. [**ResPitch**](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/-/tree/master/ResPitch)
<br>This is a short presentation aimed at "pitching" the program and introducing it to you. It will help you understand what 3D Slicer is used for, how it can be used, and whether it's the right program for your research.</br>

2. [**Workshop**](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/-/tree/master/Workshop)
<br>This is an introductory workshop to 3D Slicer. The workshop will walk you through the basic functions of 3D Slicer, and equip you with the skills you need to begin your project. Online training sessions based on these materials will also be run to support your learning.</br>



# About the Trainer

Kathy is an MD MPH student at the University of Melbourne, and is working on a research project at the Royal Women’s Hospital. She picked up 3D Slicer at the 3DMedLab at the Austin Hospital and enjoys using the program to educate, innovate and improve health outcomes. Kathy is excited to share and introduce the program to students and researchers of all disciplines. 

**Contact details**
<br>Email: kathyz@unimelb.edu.au</br>
<br>Twitter: [@kathy_zzzz](https://twitter.com/kathy_zzzz) </br>
