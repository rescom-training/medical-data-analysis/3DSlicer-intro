# What is the ResPitch?

This is a short presentation aimed at "pitching" and introducing the program 3D Slicer to you. This activity will help you understand what 3D Slicer is, what it can be used for and whether this is the right tool for your research or project. 

By completing this activity, you will: 
* learn what 3D Slicer is, 
* learn how 3D Slicer can be used, 
* learn some basic segmenting skills,
* complete some fun mini-challenges to test your learning.


# How can I do this at home?
This activity should take approximately 30 minutes. You can download this entire folder at once by clicking the download button above (left of the blue "clone" button) > download this directory > zip.

Included are the [slides](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/blob/master/RESPITCH-slides.pdf), a [cheat sheet](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/blob/274edb3811dca829cb3bdcf135fcf9907bea4f13/RESPITCH-cheat-sheet.docx) with some handy hints, and the [checklist](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/blob/master/RESPITCH-checklist.docx) for the final challenge. You can also download the [solutions](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/blob/master/solutions.zip) and view them in 3D Slicer afterwards.

We recommended that you print out the cheat sheet and checklist, so you don't have to have too many windows open at once.

# When is the next training session?
Training sessions for the ResPitch are usually only offered during special events, however the same information is covered in much more detail by the [workshop](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/-/tree/master/Workshop).

You can find the dates and times of our online training sessions on the [Research Computing Services Eventbrite](http://rescomunimelb.eventbrite.com.au/) page.
