# What is this workshop about?

This workshop is usually delivered as a two-hour, in-person <i>Introduction to 3D Slicer</i> workshop. It is intended to walk you through an expanded version of the basics of the program with challenges to test your learning along the way. It covers the principles of medical imaging, the fundamentals of using 3D Slicer and an overview of 3D printing.

The <i>Introduction to 3D Slicer</i> program is separated into <i>Visualisation</i> and <i>Segmentation</i> modules which are focused on visualising and segmenting medical data respectively. These modules are separate but complementary, and are designed so that you may attend one or both in any order.


# How can I attend an online training session?
You can find the dates and times of our online training sessions on the [Research Computing Services Eventbrite](http://rescomunimelb.eventbrite.com.au/) page. 

If you are interested in 3D data or 3D printing more widely, our <i>Digital to Physical: 3D Data and 3D Printing</i> meetups may be for you! I also highly recommend the Rhino3D workshops as a complement to 3D Slicer, to round out your knowledge of 3D data and 3D modelling.

### Do I have to prepare for an online training session?
Our online training sessions are designed to be suitable for people of all skill levels. No experience or prior knowledge required!

You will need the following:
- Laptop and charger
- Mouse recommended, but optional
- Download 3D Slicer from [here](https://download.slicer.org/).
- Download the [teaching slides](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/-/blob/master/Workshop/teaching%20slides.pdf). We highly recommend that you print them if you can to avoid having too many windows open at once during online training


# Can I do this at home, in my own time?
Slides have been made available so you can work through them at home, at your own pace.

Download the [legacy slides](https://gitlab.unimelb.edu.au/rescom-training/medical-data-analysis/3DSlicer-intro/-/blob/master/Workshop/legacy%20slides.pdf) above to get started.

**Note:** These legacy slides were made past ResComs and ResLeads. They are extremely comprehensive and are ideal work for working through at home. Although some of the images are from an older version of 3D Slicer, much of the information remains the same. We are working to update and adapt our teaching materials for e-learning and we appreciate your understanding :)


# Further resources

This [GitBook](https://lassoan.gitbooks.io/test-book/content/) is a great and comprehensive instructional manual for getting started with 3D Slicer, and complements the legacy slides.

The [3D Slicer wiki](https://www.slicer.org/wiki/Main_Page) is full of interesting information, especially about particular modules you may be interested in downloading and using.

The [3D Slicer discussion forum](https://discourse.slicer.org/) is a place you can ask questions and meet other people using 3D Slicer as well.
